<?php


class PrincetonRecreationModule extends KGOModule {

    /*
     *  The initializeForPageConfigObjects_ methods below don't need to do much, they simply check if a feed has been configured
     *  The $objects configured in the page objdefs will take control from here
     */

    protected function initializeForPageConfigObjects_index(KGOUIPage $page, $objects) {
        $jc_feeds=$this->getFeed();
        //kgo_debug($jc_feeds,true,true);
        //kgo_debug($this,true,true);
        if (!($feed = $this->getFeed())) {
            $this->setPageError($page, "no feed for recreation-index");
            return;
        }
    }
    
    protected function initializeForPageConfigObjects_info(KGOUIPage $page, $objects) {  

        return; 
    } //Can't forget to initialize the pages
    
    private function convert_info_date($date) {
        $new_date=$date;
        if(strtotime($date)) {
            $secs=strtotime($date);
            $new_date=date("D, M j, Y",$secs); //Mon, Oct 26, 2015
        }
        return $new_date;
    }
    
    public function getPURecreation() {
        //$tics=time();
        //kgo_debug("getPURecreation called at tics[$tics]",true,true);
        $this_getId=$this->getId();
        //kgo_debug($this_getId,true,true);         
        $tmp_array=array();    
        if($feed = $this->getFeed()) {
            $jc_retriever=$feed->getRetriever();
            //kgo_debug($jc_retriever,true,true);
            $jc_data=$jc_retriever->getData();
            //kgo_debug($jc_data,true,true);           
            if(is_array($jc_data)) {
                //10/23/15 - need to remove duplicates
                $jc_real_data=array();
                $building_id_defined=array();
                $cnt=count($jc_data);
                if($cnt) {
                    for($i=0;$i<$cnt;$i++) {
                        $jc_data[$i]->set_is_subcategory(0);
                        $jc_data[$i]->set_description(" ");
                        $jc_data[$i]->set_kurogo_id($this_getId);
                        $building_id=$jc_data[$i]->get_building_id();
                        if(isset($building_id_defined[$building_id]) == false) {
                            $jc_real_data[]=$jc_data[$i];
                            $building_id_defined[$building_id]=1;
                        }
                    }
                }
                //kgo_debug($jc_real_data,true,true);
                $jc_double_data=array();
                $jc_real_cnt=count($jc_real_data);
                for($i=0;$i<$jc_real_cnt;$i++) {
                    $tmp_object = new PrincetonRecreationDataObject;
                    $tmp_object->set_is_subcategory(0);
                    $tmp_object->set_description(" ");
                    $tmp_object->set_kurogo_id($this_getId);
                    $tmp_object->set_double_index(1);
                    $tmp_object->set_category_id($jc_real_data[$i]->get_category_id());
                    $tmp_object->set_building_id($jc_real_data[$i]->get_building_id());
                    $tmp_object->set_recreationname($jc_real_data[$i]->getrecreationname());
                    $jc_double_data[]=$tmp_object;
                    //kgo_debug($jc_double_data,true,true);
                    unset($tmp_object);
                    $tmp_object = new PrincetonRecreationDataObject;
                    $tmp_object->set_is_subcategory(0);
                    $tmp_object->set_description(" ");
                    $tmp_object->set_kurogo_id($this_getId);
                    $tmp_object->set_double_index(2);
                    $tmp_object->set_category_id($jc_real_data[$i]->get_category_id());
                    $tmp_object->set_building_id($jc_real_data[$i]->get_building_id());
                    $tmp_object->set_recreationname($jc_real_data[$i]->getrecreationname());
                    //kgo_debug($tmp_object,true,true);
                    $jc_double_data[]=$tmp_object;
                    unset($tmp_object);
                    //kgo_debug($jc_double_data,true,true);
                }
                //kgo_debug($jc_double_data,true,true);
                return $jc_double_data;
            } else {
                return $tmp_array;
            }
        } else {
            return $tmp_array;        
        }
    }
    
    public function getPURecreationSubCategory() {
        $jc_data=array();
        $category_id = $this->getArg('category_id');
        $my_error="";
        $my_name="";
        $my_description="";
        $my_building_id="";
        $attribute_names=array();
        $attribute_values=array();
        $good_objects=array();
        $open_status="";
        $day_names=array();
        $day_times=array();
        $zero_entry=0;
        $PLPlace_cnt=0;
        if($category_id == "") {
            $my_error="no category_id specified";
        } elseif(preg_match("/^[\d]+$/",$category_id) == 0) {
            $my_error="invalid category_id[$category_id]";
        } else {
            //$my_error="category_id[$category_id]";
            $subcategory_url="https://atsweb.princeton.edu/mobile/places/?categoryID=".$category_id;
            $curlSes = curl_init();
            curl_setopt($curlSes, CURLOPT_URL, $subcategory_url);
            curl_setopt($curlSes, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($curlSes, CURLOPT_RETURNTRANSFER, true);
            $urlData = curl_exec($curlSes);
            curl_close($curlSes);            
            if($urlData) {
                //kgo_debug($urlData,true,true);
                try {
                    $xml = new SimpleXMLElement($urlData, LIBXML_NOCDATA);
                    $data = json_decode(json_encode($xml), true);                    
                    //kgo_debug($data,true,true);
                    $break="<br />";
                    if(isset($data['categories'])) {
                        $TCCategory=$data['categories'];
                        if(isset($TCCategory['TCCategory'])) {
                            $my_TCCategory=$TCCategory['TCCategory'];
                            //kgo_debug($my_TCCategory,true,true);
                            $my_name=$my_TCCategory['name'];
                            $my_category_id=$my_TCCategory["category_id"];
                            if($my_category_id != $category_id) {
                                $my_error="my_category_id[$my_category_id]not equal to category_id[$category_id]";
                            } else {
                                if(isset($data['places'])) {
                                    $places=$data['places'];
                                    if(isset($places['PLPlace'])) {
                                        $PLPlace=$places['PLPlace'];
                                        //kgo_debug($PLPlace,false,false);
                                        $what_PLPlace=array();
                                        if(isset($PLPlace[0])) {
                                            $PLPlace_cnt=count($PLPlace);
                                            //kgo_debug("PLPlace_cnt[$PLPlace_cnt]",false,false);
                                            $what_PLPlace=$PLPlace[0];
                                            $zero_entry=1;
                                        } elseif(isset($PLPlace['description'])) {
                                            $what_PLPlace=$PLPlace;
                                            $PLPlace_cnt=1;
                                        } else {
                                            $my_error="no 0 or description field in PLPlace";
                                        }
                                    } else {
                                        $my_error="no PLPlace field in places";
                                    }
                                    
                                    if($my_error == "") {
                                        if(isset($what_PLPlace['description'])) {
                                            $my_description=$what_PLPlace['description'];
                                            if(isset($what_PLPlace['building_id'])) {
                                                $my_building_id=$what_PLPlace['building_id'];
                                                //kgo_debug($what_PLPlace,true,true);
                                                if(isset($what_PLPlace['open'])) {
                                                    if(preg_match("/yes/i",$what_PLPlace['open'])) {
                                                        $open_status=" is currently open";
                                                    } elseif(preg_match("/no/i",$what_PLPlace['open'])) {
                                                        $open_status=" is currently not open";
                                                    } else {
                                                        $open_status=" unknown open_status[".$what_PLPlace['open']."]";
                                                    }
                                                }
                                                if(isset($what_PLPlace['times'])) {
                                                    $times=$what_PLPlace['times'];
                                                    //kgo_debug($times,true,true);
                                                    if(isset($times['day'])) {
                                                        $days_array=$times['day'];
                                                        //kgo_debug($days_array,true,true);
                                                        if(is_array($days_array)) {
                                                            foreach($days_array as $day_key=>$day_value) {
                                                                $session_info="";
                                                                $what_day=$day_value['name'];
                                                                $what_date=$day_value['date'];
                                                                $what_sessions=$day_value['sessions'];
                                                                if($what_day == "") {
                                                                    $my_error="no what_day value for day_key[$day_key]";
                                                                } elseif($what_date == "") {
                                                                    $my_error="no what_date value for day_key[$day_key]";
                                                                } elseif(preg_match("/^monday|tuesday|wednesday|thursday|friday|saturday|sunday$/i",$what_day) == 0) {
                                                                    $my_error="unknown what_day[$what_day]for day_key[$day_key]";
                                                                } elseif(preg_match("/^[\d]{4}-[\d]{2}-[\d]{2}$/",$what_date) == 0) {
                                                                    $my_error="unexpected date format for what_date[$what_date]for day_key[$day_key]";
                                                                } else {
                                                                    $what_date=self::convert_info_date($what_date);
                                                                    //kgo_debug($what_sessions,false,false);
                                                                    if(isset($what_sessions['session'])) {
                                                                        $my_session=$what_sessions['session'];
                                                                        if(isset($my_session['hourset'])) {
                                                                            $session_info=$break.$my_session['hourset'];
                                                                        } elseif(isset($my_session[0])) {
                                                                            $tmp_array=array();
                                                                            $tmp_cnt=count($my_session);
                                                                            for($n=0;$n<$tmp_cnt;$n++) {
                                                                                $tmp_name=$my_session[$n]['name'];
                                                                                $tmp_hour=$my_session[$n]['hourset'];
                                                                                $tmp_array[]="$tmp_name $tmp_hour";
                                                                            }
                                                                            $session_info=$break.implode($break,$tmp_array);
                                                                        }
                                                                    } else {
                                                                        //$my_error="no session field in sessions for day_key[$day_key]";
                                                                        $session_info=$break."Closed";
                                                                    }
                                                                    if($my_error == "") {
                                                                        $day_names[]="$what_date $session_info";
                                                                    }
                                                                }
                                                                if($my_error != "") {
                                                                    break;
                                                                }
                                                            }
                                                        } else {
                                                            $my_error="days_array is not an array in times";
                                                        }
                                                    } else {
                                                        $my_error="no day field in times";
                                                    }
                                                }
                                                if($my_error == "") {
                                                    if(isset($what_PLPlace['attributes'])) {
                                                        $attributes=$what_PLPlace['attributes'];
                                                        //kgo_debug($attributes,true,true);
                                                        if(isset($attributes['attribute'])) {
                                                            $attribute_array=$attributes['attribute'];
                                                            //kgo_debug($attribute_array,false,false);
                                                            if(isset($attribute_array[0])) {
                                                                $attribute_cnt=count($attribute_array);
                                                                //kgo_debug($attribute_cnt,false,false);
                                                                for($j=0;$j<$attribute_cnt;$j++) {
                                                                    $attribute_name=$attribute_array[$j]['name'];
                                                                    $attribute_value=$attribute_array[$j]['value'];
                                                                    if($attribute_name == "") {
                                                                        $my_error="no j[$j]attribute_array name in attribute field in attributes in zero_entry[$zero_entry]what_PLPlace";
                                                                    } elseif($attribute_value == "") {
                                                                        $my_error="no j[$j]attribute_array  value in attribute field in attributes in zero_entry[$zero_entry]what_PLPlace";
                                                                    } else {
                                                                        $attribute_names[]=$attribute_name;
                                                                        $attribute_values[]=$attribute_value;
                                                                    }
                                                                    if($my_error != "") {
                                                                        break;
                                                                    }
                                                                }
                                                            } elseif(is_array($attribute_array)) {
                                                                $my_attribute_cnt=count($attribute_array);
                                                                if($my_attribute_cnt <=3) {
                                                                    $attribute_name=$attribute_array['name'];
                                                                    $attribute_value=$attribute_array['value'];
                                                                    if($attribute_name == "") {
                                                                        $my_error="no j[$j]attribute_array name in attribute field in attributes in zero_entry[$zero_entry]what_PLPlace";
                                                                    } elseif($attribute_value == "") {
                                                                        $my_error="no j[$j]attribute_array  value in attribute field in attributes in zero_entry[$zero_entry]what_PLPlace";
                                                                    } else {
                                                                        $attribute_names[]=$attribute_name;
                                                                        $attribute_values[]=$attribute_value;
                                                                    }
                                                                } else {
                                                                    $my_error="my_atribute_cnt[$my_attribute_cnt]is greater than 3";
                                                                }
                                                            } else {
                                                                $my_error="attribute_array is not an array in attribute field in attributes in zero_entry[$zero_entry]what_PLPlace";
                                                            }
                                                        }
                                                    } else {
                                                        $my_error="no attributes field in zero_entry[$zero_entry]what_PLPlace";
                                                    }
                                                }
                                            } else {
                                                $my_error="no building_id in zero_entry[$zero_entry]what_PLPlace ";
                                            }
                                        } else {
                                            $my_error="no description in zero_entry[$zero_entry]what_PLPlace ";
                                        }
                                    }
                                    if($my_error == "") {
                                        if($PLPlace_cnt == 1) {
                                            $object = new PrincetonRecreationDataObject();
                                            $object->set_is_subcategory(1);
                                            $object->set_description($my_description);
                                            $object->set_recreationname("$my_name $open_status");
                                            $good_objects[]=$object;
                                            $my_attribute_cnt=count($attribute_names);
                                            //kgo_debug("my_attribute_cnt[$my_attribute_cnt]",false,false);
                                            if($my_attribute_cnt) {
                                                for($j=0;$j<$my_attribute_cnt;$j++) {
                                                    $object = new PrincetonRecreationDataObject();
                                                    $object->set_is_subcategory(1);
                                                    $object->set_description($attribute_values[$j]);
                                                    $object->set_recreationname($attribute_names[$j]);
                                                    $good_objects[]=$object;
                                                }
                                            }
                                            $day_names_cnt=count($day_names);
                                            if($day_names_cnt) {
                                                $object = new PrincetonRecreationDataObject();
                                                $object->set_is_subcategory(1);
                                                $object->set_recreationname("Hours:");
                                                $time_info=implode($break,$day_names);
                                                $object->set_description($time_info);
                                                $good_objects[]=$object;
                                            }
                                        } else {
                                            $object = new PrincetonRecreationDataObject();
                                            $object->set_is_subcategory(1);
                                            $object->set_recreationname("$my_name $open_status");
                                            $tmp_lines=array();
                                            $tmp_lines[]=$my_description;
                                            $my_attribute_cnt=count($attribute_names);
                                            //kgo_debug("my_attribute_cnt[$my_attribute_cnt]",false,false);
                                            if($my_attribute_cnt) {
                                                for($j=0;$j<$my_attribute_cnt;$j++) {
                                                    $tmp_array=array( " ",$attribute_names[$j],$attribute_values[$j]);
                                                    $tmp_lines[]=implode($break,$tmp_array);
                                                }
                                            }
                                            $day_names_cnt=count($day_names);
                                            if($day_names_cnt) {
                                                $tmp_lines[]="$break Hours:";
                                                for($j=0;$j<$day_names_cnt;$j++) {
                                                    $tmp_lines[]=$day_names[$j];
                                                }
                                            }
                                            $tmp_info=implode($break,$tmp_lines);
                                            $object->set_description($tmp_info);
                                            $good_objects[]=$object;
                                        }
                                    }
                                    if(($my_error == "")  && ($zero_entry) && ($PLPlace_cnt > 1)) {
                                        for($i=1;$i<$PLPlace_cnt;$i++) {
                                            //kgo_debug("i[$i]",false,false);
                                            $my_name="";
                                            $my_description="";
                                            $my_building_id="";
                                            $attribute_names=array();
                                            $attribute_values=array();
                                            $open_status="";
                                            $day_names=array();
                                            $day_times=array();
                                            $what_PLPlace=$PLPlace[$i];
                                            if(isset($what_PLPlace['name'])) {
                                                $my_name=$what_PLPlace['name'];
                                                //kgo_debug($my_name,false,false);
                                                if(isset($what_PLPlace['description'])) {
                                                    $my_description=$what_PLPlace['description'];
                                                    if(isset($what_PLPlace['building_id'])) {
                                                        $my_building_id=$what_PLPlace['building_id'];
                                                        //kgo_debug($what_PLPlace,true,true);
                                                        if(isset($what_PLPlace['open'])) {
                                                            if(preg_match("/yes/i",$what_PLPlace['open'])) {
                                                                $open_status=" is currently open";
                                                            } elseif(preg_match("/no/i",$what_PLPlace['open'])) {
                                                                $open_status=" is currently not open";
                                                            } else {
                                                                $open_status=" unknown open_status[".$what_PLPlace['open']."]";
                                                            }
                                                        }
                                                        if(isset($what_PLPlace['times'])) {
                                                            $times=$what_PLPlace['times'];
                                                            //kgo_debug($times,true,true);
                                                            if(isset($times['day'])) {
                                                                $days_array=$times['day'];
                                                                //kgo_debug($days_array,true,true);
                                                                if(is_array($days_array)) {
                                                                    foreach($days_array as $day_key=>$day_value) {
                                                                        $session_info="";
                                                                        $what_day=$day_value['name'];
                                                                        $what_date=$day_value['date'];
                                                                        $what_sessions=$day_value['sessions'];
                                                                        if($what_day == "") {
                                                                            $my_error="no what_day value for day_key[$day_key]in i[$i]PLPlace";
                                                                        } elseif($what_date == "") {
                                                                            $my_error="no what_date value for day_key[$day_key]in i[$i]PLPlace";
                                                                        } elseif(preg_match("/^monday|tuesday|wednesday|thursday|friday|saturday|sunday$/i",$what_day) == 0) {
                                                                            $my_error="unknown what_day[$what_day]for day_key[$day_key]in i[$i]PLPlace";
                                                                        } elseif(preg_match("/^[\d]{4}-[\d]{2}-[\d]{2}$/",$what_date) == 0) {
                                                                            $my_error="unexpected date format for what_date[$what_date]for day_key[$day_key]in i[$i]PLPlace";
                                                                        } else {
                                                                            $what_date=self::convert_info_date($what_date);
                                                                            //kgo_debug($what_sessions,false,false);
                                                                            if(isset($what_sessions['session'])) {
                                                                                $my_session=$what_sessions['session'];
                                                                                if(isset($my_session['hourset'])) {
                                                                                    $session_info=$break.$my_session['hourset'];
                                                                                } elseif(isset($my_session[0])) {
                                                                                    $tmp_array=array();
                                                                                    $tmp_cnt=count($my_session);
                                                                                    for($n=0;$n<$tmp_cnt;$n++) {
                                                                                        $tmp_name=$my_session[$n]['name'];
                                                                                        $tmp_hour=$my_session[$n]['hourset'];
                                                                                        $tmp_array[]="$tmp_name $tmp_hour";
                                                                                    }
                                                                                    $session_info=$break.implode($break,$tmp_array);
                                                                                }
                                                                            } else {
                                                                                //$my_error="no session field in sessions for day_key[$day_key]";
                                                                                $session_info=$break."Closed";
                                                                            }
                                                                            if($my_error == "") {
                                                                                $day_names[]="$what_date $session_info";
                                                                            }
                                                                        }
                                                                        if($my_error != "") {
                                                                            break;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $my_error="days_array is not an array in times";
                                                                }
                                                            } else {
                                                                $my_error="no day field in times";
                                                            }
                                                        }
                                                    } else {
                                                        $my_error="no building id in i[$i]PLPlace";
                                                    }
                                                } else { 
                                                    $my_error="no description field in i[$i]PLPlace";
                                                }
                                            } else {
                                                $my_error="no name field in i[$i]PLPlace";
                                            }
                                            if($my_error == "") {
                                                $object = new PrincetonRecreationDataObject();
                                                $object->set_is_subcategory(1);
                                                $object->set_recreationname("$my_name $open_status");
                                                $tmp_lines=array();
                                                $tmp_lines[]=$my_description;
                                                $my_attribute_cnt=count($attribute_names);
                                                //kgo_debug("my_attribute_cnt[$my_attribute_cnt]",false,false);
                                                if($my_attribute_cnt) {
                                                    for($j=0;$j<$my_attribute_cnt;$j++) {
                                                        $tmp_array=array( " ",$attribute_names[$j],$attribute_values[$j]);
                                                        $tmp_lines[]=implode($break,$tmp_array);
                                                    }
                                                }
                                                $day_names_cnt=count($day_names);
                                                if($day_names_cnt) {
                                                    $tmp_lines[]="$break Hours:";
                                                    for($j=0;$j<$day_names_cnt;$j++) {
                                                        $tmp_lines[]=$day_names[$j];
                                                    }
                                                }
                                                $tmp_info=implode($break,$tmp_lines);
                                                $object->set_description($tmp_info);
                                                $good_objects[]=$object;
                                            } else {
                                                break;
                                            }
                                        }
                                    }                                   
                                } else {
                                    $my_error="no places field in data";
                                }
                            }                         
                        } else {
                            $my_error="no TCCategory field in TCCategory";
                        }
                    } else {
                        $my_error="no categories field in data";
                    }
                } catch (Exception $e) {
                    //$this->response->setResponseError($e->getMessage());
                    $parse_error=$e->getMessage();
                    $my_error="could not parse XML data-parse_error[$parse_error]from curl";
                }    
            } else {
                $my_error="subcategory_url[$subcategory_url]could fetch subcategory info for category_id[$category_id] via curl"; 
            }
        }
        if($my_error == "") {
            if(count($good_objects) == 0) {
                $my_error="no errors but no good_objects";
            }
        }
        if($my_error != "") {
            $object = new PrincetonRecreationDataObject();
            $object->set_is_subcategory(1);
            $object->set_recreationname("ERROR_CONDITION");
            $object->set_description($my_error);
            $jc_data[]=$object;
        } else {
            $jc_data=array_merge($jc_data,$good_objects);
        }
        //kgo_debug($jc_data,true,true);
        return $jc_data;
    }
    
}
