<?php

class PrincetonRecreationDataObject extends KGODataObject
{
    const NAME_ATTRIBUTE = 'pu:name';
    const CATEGORY_ID_ATTRIBUTE = 'pu:category_id';
    const KUROGO_ID_ATTRIBUTE = 'pu:kurogo_id';
    const DESCRIPTION_ATTRIBUTE='pu:description';
    const IS_SUBCATEGORY_ATTRIBUTE='pu:is_subcategory';
    const BUILDING_ID_ATTRIBUTE = 'pu:building_id';
    const DOUBLE_INDEX_ATTRIBUTE = 'pu:double_index';


    public function getrecreationname() {
        return $this->getAttribute(self::NAME_ATTRIBUTE);
    }
    
    public function get_category_id() {
        return $this->getAttribute(self::CATEGORY_ID_ATTRIBUTE);
    }

    public function get_kurogo_id() {
        return $this->getAttribute(self::KUROGO_ID_ATTRIBUTE);
    }
    
    public function get_is_subcategory() {
        return $this->getAttribute(self::IS_SUBCATEGORY_ATTRIBUTE);
    }
    
    public function get_description() {
        return $this->getAttribute(self::DESCRIPTION_ATTRIBUTE);
    }
    
    public function get_building_id() {
        return $this->getAttribute(self::BUILDING_ID_ATTRIBUTE);
    }
    
    public function get_double_index() {
        return $this->getAttribute(self::DOUBLE_INDEX_ATTRIBUTE);
    }
    
    public function set_recreationname($name) {
        $this->setAttribute(self::NAME_ATTRIBUTE,$name);
    }
    
    public function set_category_id($category_id) {
        $this->setAttribute(self::CATEGORY_ID_ATTRIBUTE,$category_id);
    }
    
    public function set_kurogo_id($id_value) {
        $this->setAttribute(self::KUROGO_ID_ATTRIBUTE,$id_value);    
    }
    
    public function set_is_subcategory($is_subcategory) {
        $this->setAttribute(self::IS_SUBCATEGORY_ATTRIBUTE,$is_subcategory);
    }
    
    public function set_description($description) {
        $this->setAttribute(self::DESCRIPTION_ATTRIBUTE,$description);
    }
    
    public function set_building_id($building_id) {
        $this->setAttribute(self::BUILDING_ID_ATTRIBUTE,$building_id);
    }
    
    public function set_double_index($double_index) {
        $this->setAttribute(self::DOUBLE_INDEX_ATTRIBUTE,$double_index);
    }

    public function getUIField(KGOUIObject $object, $field) {
        //kgo_debug($field,false,false);       
        switch ($field) {            
            case 'url':
            $kurogo_id=$this->get_kurogo_id();
            $is_subcategory=$this->get_is_subcategory();
            $double_index=$this->get_double_index();
            //kgo_debug("kurogo_id[$kurogo_id]double_index[$double_index]",true,true);
            $realURL="";
            if($double_index == 2) {
                $category_id=$this->get_category_id();
                $my_URL=KGOURL::createForModuleWebPage($kurogo_id, 'info', array('category_id' => $category_id)); 
                //kgo_debug($my_URL,false,false);
                $realURL = KGOURL::inflate($my_URL); //Must call inflate on a string URL in order to make it a KGOURL otherwise errors are thrown
                return $realURL;
            } elseif($double_index == 1) {
                $building_id=$this->get_building_id();
                $my_URL=KGOURL::createForModuleWebPage('map', 'search', array('filter' => $building_id,'feed'=>'*')); 
                //kgo_debug($my_URL,true,true);
                $realURL = KGOURL::inflate($my_URL); //Must call inflate on a string URL in order to make it a KGOURL otherwise errors are thrown
                return $realURL;
            }
            return parent::getUIField($object,$field);   
            case 'title':
                $name=$this->getrecreationname();
                $double_index=$this->get_double_index();
                if($double_index == 1) {
                    $name="Find $name";
                } elseif($double_index == 2) {
                    $name="Get Schedule Info for $name";
                }
                return $name;
            case 'subtitle':
                $is_subcategory=$this->get_is_subcategory();
                $description=$this->get_description();
                if($is_subcategory) {
                    $what_value = $description;
                } else {
                    $kurogo_id=$this->get_kurogo_id();
                    $category_id=$this->get_category_id();
                    $building_id=$this->get_building_id();
                    $my_URL=KGOURL::createForModuleWebPage($kurogo_id, 'info', array('category_id' => $category_id)); 
                    //10/26/15 - per Wil's instruction, use getHTTPString
                    $deflate_URL=$my_URL->getHTTPString();
                    //kgo_debug("deflate_URL[$deflate_URL]",true,true);
                    $what_value="<a href=\"$deflate_URL\">Get Schedule Info</a>";
                    $what_value=$description;
                }
                return $what_value;
            default:
                return parent::getUIField($object, $field);
        }
    }
}
